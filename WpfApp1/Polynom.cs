﻿using System;
namespace WpfApp1
{
    public class Polynom
    {
        public Polynom()
        {
            length = 1;
            coeffs = new double[1];
            coeffs[0] = 0;
        }
        public Polynom(double[] coeff, int len)
        {
            length = len;
            coeffs = new double[length];
            for (int i = 0; i < length; i++)
                coeffs[i] = coeff[i];
        }
        public Polynom(Polynom polynom)
        {
            length = polynom.length;
            coeffs = new double[length];
            for (int i = 0; i < length; i++)
                coeffs[i] = polynom.coeffs[i];
        }
        public int getLength() { return length; }
        public void print()
        {
            for (int i = length - 1; i >= 0; i--)
            {
                if (coeffs[i] != 0)
                {
                    Console.Write(coeffs[i]);
                    Console.Write("x^");
                    Console.Write(i);
                    Console.Write("+");
                }
            }
            Console.WriteLine();
        }
        public double getElemAt(int index) {
            return coeffs[index];
        }
        public void addElem(double elem, int idx) {
            coeffs[idx] = elem;
        }
        public Polynom sum(Polynom p)
        {
            int min_l;
            bool deg_f_bigger;
            if (length > p.getLength())
            {
                min_l = p.getLength();
                deg_f_bigger = true;
            }
            else
            {
                min_l = length;
                deg_f_bigger = false;
            }
            for (int i = 0; i < min_l; i++)
            {
                if (deg_f_bigger) coeffs[i] += p.coeffs[i];
                else p.coeffs[i] += coeffs[i];
            }
            if (deg_f_bigger) return (this);
            else return (p);
        }
        public Polynom sub(Polynom p)
        {
            int min_l;
            bool deg_f_bigger;
            if (length > p.getLength())
            {
                min_l = p.getLength();
                deg_f_bigger = true;
            }
            else
            {
                min_l = length;
                deg_f_bigger = false;
            }

            for (int i = 0; i < p.length; i++)
            {//g(x)=-g(x) присваивание
                p.coeffs[i] = -p.coeffs[i];
            }

            for (int i = 0; i < min_l; i++)
            {
                if (deg_f_bigger) coeffs[i] += p.coeffs[i];
                else p.coeffs[i] += coeffs[i];
            }
            if (deg_f_bigger) return (this);
            else return (p);
        }
        public Polynom mult(Polynom p)
        {
            double[] coeff = new double[length + p.length - 1];
            Polynom tmp = new Polynom(coeff, length + p.length - 1);
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < p.length; j++)
                {
                    tmp.coeffs[i + j] += coeffs[i] * p.coeffs[j];
                }

            }
            return tmp;
        }
        public Polynom div_whole_part(Polynom divider, Polynom dividend, Polynom curr_res)
        {
            int res_deg = divider.length - dividend.length + 1;

            Polynom res = new Polynom(new double[res_deg], res_deg);

            int divider_curr_deg = divider.length - 1;

            while (true)
            {

                if (divider_curr_deg < dividend.length - 1) return curr_res;
                int idx = divider.length - 1 - divider_curr_deg;
                if (divider.coeffs[divider_curr_deg] != 0)
                {
                    res.coeffs[res_deg - idx - 1] =
                       divider.coeffs[divider_curr_deg] / dividend.coeffs[dividend.length - 1];
                    if (curr_res == null)
                        return div_whole_part(divider.sub(dividend.mult(res)), dividend, res);
                    else
                        return div_whole_part(divider.sub(dividend.mult(res)), dividend, res.sum(curr_res));
                }
                else divider_curr_deg--;
            }
        }
        public Polynom div_frac_part(Polynom p, Polynom q)
        {
            return p.sub(div_whole_part(p, q, null).mult(q));
        }
        private double[] coeffs;
        private int length;
    };
}