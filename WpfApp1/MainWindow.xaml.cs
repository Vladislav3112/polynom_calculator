﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int ariph_operation;
        
        public MainWindow()
        {
            InitializeComponent();
        }



        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (RadioBtnSum.IsChecked==true)
            {
                TextBlockOperation.Text = " +";
                ariph_operation = 0;

            }
            else if (RadioBtnSub.IsChecked == true)
            {
                TextBlockOperation.Text = " -";
                ariph_operation = 1;
            }
            else if (RadioBtnMult.IsChecked == true)
            {
                TextBlockOperation.Text = " *";
                ariph_operation = 2;
            }
            else if (RadioBtnDiv.IsChecked == true)
            {
                TextBlockOperation.Text = " /";
                ariph_operation = 3;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int f_length = 0;
            int g_length = 0;
            String f = "";
            String g = "";
            try
            {
                f_length = Convert.ToInt32(TextBoxFdeg.Text) + 1;
                g_length = Convert.ToInt32(TextBoxGdeg.Text) + 1;
                f = Convert.ToString(polynom_f.Text);
                g = Convert.ToString(polynom_g.Text);
            }
            catch (Exception err) {
                MessageBox.Show("Incorrrect degree!");
                Environment.Exit(-1);
            }

            double[] coeff_f = new double[f_length];
            for (int i = 0; i < f_length; i++) coeff_f[i] = 0;
            double[] coeff_g = new double[g_length];
            for (int i = 0; i < g_length; i++) coeff_g[i] = 0;

            //Polynom

            Polynom ReadPolynom(double[]coeffs,int poly_length,String polynom){     
                String str_deg, str_coeff;
                Polynom p = new Polynom(coeffs, poly_length);
                while (polynom!="")
                    {
                        int idx = polynom.IndexOf("x");

                    if (idx != -1)
                    {
                        str_coeff = polynom.Substring(0, idx);
                        polynom = polynom.Remove(0, idx);//delete read coeff
                        int pidx = polynom.IndexOf("+");
                        int midx = polynom.IndexOf("-");
                        if ((midx < pidx || pidx == -1)&& midx!=-1) pidx = midx;//position of clothest sign

                        try
                        {
                            str_deg = polynom.Substring(2, pidx - 2);
                            coeffs[int.Parse(str_deg)] += double.Parse(str_coeff);
                        }
                        catch (Exception err) {
                           MessageBox.Show("Incorrect input");
                           Environment.Exit(-1);
                        }
                        
                        //delete read deg
                        polynom = polynom.Remove(0, pidx);
                    }
                    else
                    {
                        coeffs[0] = Convert.ToDouble(polynom);
                        polynom = "";
                    }

                    }
                for (int i = 0; i < p.getLength(); i++) {
                    p.addElem(coeffs[i], i);
                }
                return p;
            }


            Polynom f1 = ReadPolynom(coeff_f, f_length, f);
            Polynom g1 = ReadPolynom(coeff_g, g_length, g);
            String WritePolynom(Polynom poly) {
                string s = "";
                int k = 0;
                for (int i = poly.getLength()-1; i>=0; i--) {
                    if (poly.getElemAt(i) != 0) {
                        if (k != 0 && poly.getElemAt(i) > 0) s += "+";    //sign check 
                        s += poly.getElemAt(i);
                        if (i != 0)
                        {
                            s += "x^";
                            s += i;
                        } 
                    k++; }
                }
                return (s);
            }


            if (ariph_operation == 0) TextBox_res.Text = WritePolynom(f1.sum(g1));
            if (ariph_operation == 1) TextBox_res.Text = WritePolynom(f1.sub(g1));
            if (ariph_operation == 2) TextBox_res.Text = WritePolynom(f1.mult(g1));
            if (ariph_operation == 3) TextBox_res.Text =
                                            WritePolynom(f1.div_whole_part(f1, g1, null)) + 
                                            "+("+
                                            WritePolynom(f1.div_frac_part(f1, g1)) +
                                            ") / (" +
                                            WritePolynom(g1)
                                            +")"
            ;
        }
    }
}
